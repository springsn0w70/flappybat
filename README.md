To open this project, install Android Studio and import the FlappyBat folder as a new project.

Licensed under GPLv3. I chose this license because it's a free, copyleft license that requires the sharing of source code and promotes development freedom. 