package com.example.flappybat;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.text.SimpleDateFormat;
import java.util.List;

public class ScoreboardSprite {
    private int scoreboardWidth = 900;
    private int scoreboardHeight = 1200;
    private List<GameScore> scores;
    private GameScore currentScore;

    public ScoreboardSprite(List<GameScore> scores, GameScore score) {
        this.scores = scores;
        this.currentScore = score;
    }

    public void draw (Canvas canvas) {
        Paint boardPaint = new Paint();
        Paint textPaint = new Paint();

        boardPaint.setColor(Color.argb(180, 102, 102, 102));
        textPaint.setColor(Color.rgb(255, 255, 255));
        textPaint.setTextSize(60);

        canvas.drawRect((GameView.screenWidth / 2) - (scoreboardWidth / 2),
                (GameView.screenHeight / 2) - (scoreboardHeight / 2),
                (GameView.screenWidth / 2) + (scoreboardWidth / 2),
                (GameView.screenHeight / 2) + (scoreboardHeight / 2),
                boardPaint);

        canvas.drawText("Top 15 scores (touch to restart)", (GameView.screenWidth / 2) - (scoreboardWidth / 2),
                (GameView.screenHeight / 2) - (scoreboardHeight / 2), textPaint);

        int yCounter = 70;
        for (GameScore s : scores) {
            // Paint current score in yellow if it's in the top 15
            if (s.points == currentScore.points) {
                textPaint.setColor(Color.rgb(255, 255, 0));
            }
            else {
                textPaint.setColor(Color.rgb(255, 255, 255));
            }

            canvas.drawText(Integer.toString(s.points),
                    (GameView.screenWidth / 2) - (scoreboardWidth / 2),
                    (GameView.screenHeight / 2) - (scoreboardHeight / 2) + yCounter,
                    textPaint);
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            canvas.drawText(formatter.format(s.dateTime),
                    (GameView.screenWidth / 2) - (scoreboardWidth / 2) + 300,
                    (GameView.screenHeight / 2) - (scoreboardHeight / 2) + yCounter,
                    textPaint);
            yCounter += 60;
        }
    }
}
