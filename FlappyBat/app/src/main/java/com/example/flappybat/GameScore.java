package com.example.flappybat;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity
public class GameScore {
    // Auto gen id
    @PrimaryKey(autoGenerate = true)
    public int gameScoreId;

    @ColumnInfo(name = "player_name")
    public String playerName;

    @ColumnInfo(name = "points")
    public int points;

    @ColumnInfo(name = "date_time")
    public Date dateTime;
}
