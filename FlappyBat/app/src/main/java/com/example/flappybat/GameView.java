package com.example.flappybat;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.room.Room;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    // Private
    private MainThread thread;
    private CharacterSprite characterSprite;
    private ScoreboardSprite scoreboardSprite;
    private StalagSprite stalag1;
    private StalagSprite stalag2;
    private StalagSprite stalag3;
    private PointsSprite pointsSprite;
    private boolean alive;
    private int points;
    StalagSprite lastGeneratedStalag = null;
    MediaPlayer jumpSound;

    // Private constant
    private final int gapWidth = 400;

    // Public
    public boolean falling = true;
    public static int gameVelocity = 10;

    // Public constant
    public static final int screenHeight =
            Resources.getSystem().getDisplayMetrics().heightPixels;
    public static final int screenWidth =
            Resources.getSystem().getDisplayMetrics().widthPixels;
    public static final int stalagWidth = 150;
    public static final int stalagHeight =
            Resources.getSystem().getDisplayMetrics().heightPixels / 2;
    public static final int gapHeight = 700;
    public static final int characterWidth = 120;
    public static final int characterHeight = 120;

    public GameView(Context context) {
        super(context);
        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);
        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread.setRunning(true);
        thread.start();
        alive = true;

        // Make character sprite
        characterSprite = new CharacterSprite(getResizedBitmap(
                BitmapFactory.decodeResource(getResources(),R.drawable.bat),
                characterWidth, characterHeight));

        // Make initial level
        Bitmap bmp;
        Bitmap bmp2;
        bmp = getResizedBitmap(BitmapFactory.decodeResource(
                getResources(), R.drawable.stalagtite), stalagWidth, stalagHeight);
        bmp2 = getResizedBitmap(BitmapFactory.decodeResource(
                getResources(), R.drawable.stalagmite), stalagWidth, stalagHeight);
        stalag1 = new StalagSprite(bmp, bmp2, 0,  0);
        stalag2 = new StalagSprite(bmp, bmp2, 0, 0);
        stalag3 = new StalagSprite(bmp, bmp2, 0, 0);

        // Make points sprite
        pointsSprite = new PointsSprite();

        jumpSound = MediaPlayer.create(getContext(), R.raw.mario_jump);

        // Reset level
        resetLevel();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    public void update() {
        if (alive) {
            // If character has reached max jump height, go back to falling
            if (!falling) {
                if (characterSprite.initialY - characterSprite.y >= characterSprite.jumpHeight) {
                    falling = true;
                }
            }

            // Update sprites
            pointsSprite.update(points);
            characterSprite.update(falling);
            stalag1.update();
            stalag2.update();
            stalag3.update();

            gameLogic();
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            // Draw everything
            characterSprite.draw(canvas);
            stalag1.draw(canvas);
            stalag2.draw(canvas);
            stalag3.draw(canvas);
            pointsSprite.draw(canvas);

            // Calculate new hitboxes
            characterSprite.calculateHitBox();
            stalag1.calculateHitBox();
            stalag2.calculateHitBox();
            stalag3.calculateHitBox();

            // Draw scoreboard when dead
            if (!alive) {
                scoreboardSprite.draw(canvas);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
            // In-game touch action makes the character jump
            if (alive) {
                // Play jump sound
                if (jumpSound != null && jumpSound.isPlaying()) {
                    jumpSound.seekTo(0);
                }
                else {
                    jumpSound.start();
                }

                falling = false;
                // Set variable for tracking jump
                characterSprite.initialY = characterSprite.y;
                // Reset momentum for after the jump
                characterSprite.yVelocity = characterSprite.minSpeed;
            }
            // Touch action when dead closes the scoreboard and restarts the game
            else {
                resetLevel();
            }
        }

        return true;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap =
                Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();

        return resizedBitmap;
    }

    public void gameLogic() {
        List<StalagSprite> stalags = new ArrayList<>();
        stalags.add(stalag1);
        stalags.add(stalag2);
        stalags.add(stalag3);

        for (StalagSprite stalag : stalags) {
            // Detect if the character is touching one of the stalags
            if (Rect.intersects(stalag.hitBoxTopStalag, characterSprite.hitBox)
            || Rect.intersects(stalag.hitBoxBottomStalag, characterSprite.hitBox)) {
                endGame();
            }

            // Detect if the pipe has gone off the left of the
            // screen and regenerate further ahead
            if (stalag.xCoord + stalagWidth < 0) {
                int newX = screenWidth + ThreadLocalRandom.current().nextInt(100, 400);
                int newY = ThreadLocalRandom.current().nextInt(-500, 500);

                // Make sure stalags aren't too close together
                if (lastGeneratedStalag != null) {
                    int gap = newX - lastGeneratedStalag.xCoord;
                    if (gap < gapWidth) {
                        newX += (gapWidth - gap);
                    }
                }

                stalag.xCoord = newX;
                stalag.yOffset = newY;

                lastGeneratedStalag = stalag;
            }
        }

        // Detect if the character has gone off the
        // bottom or top of the screen
        if (characterSprite.y + 240 < 0) {
            endGame();
        }
        if (characterSprite.y > screenHeight) {
            endGame();
        }

        // Keep adding points while player lives
        points += 1;
    }

    public void resetLevel() {
        // Reset everything to default values
        characterSprite.yVelocity = characterSprite.minSpeed;
        gameVelocity = 10;
        points = 0;
        characterSprite.y = 700;
        characterSprite.x = 100;
        stalag1.xCoord = 500;
        stalag1.yOffset = 200;
        stalag2.xCoord = 900;
        stalag2.yOffset = -100;
        stalag3.xCoord = 1300;
        stalag3.yOffset = 400;
        falling = true;
        alive = true;

        // Recalculate hitboxes with new locations so the game doesn't
        // immediately end again
        characterSprite.calculateHitBox();
        stalag1.calculateHitBox();
        stalag2.calculateHitBox();
        stalag3.calculateHitBox();
    }

    public void endGame() {
        alive = false;
        // Stop sprites from moving while scoreboard is on screen
        gameVelocity = 0;

        AppDatabase db = Room.databaseBuilder(getContext(), AppDatabase.class, "database-name").allowMainThreadQueries().build();

        GameScore score = new GameScore();
        score.points = points;
        score.dateTime = new Date();
        db.gameScoreDao().insert(score);
        List<GameScore> gameScores = db.gameScoreDao().getAll();
        db.close();

        scoreboardSprite = new ScoreboardSprite(gameScores, score);
    }
}
