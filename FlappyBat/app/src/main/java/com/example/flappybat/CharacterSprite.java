package com.example.flappybat;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import androidx.constraintlayout.solver.widgets.Rectangle;

public class CharacterSprite {
    private Bitmap image;
    public int y, x;
    public int yVelocity;
    public int accelerationRate = 1;
    public int maxSpeed = 30;
    public int minSpeed = 3;
    public int jumpHeight = 250;
    private int jumpSpeed = 15;
    public int initialY;
    private int hitBoxInwardOffset = 25;
    public Rect hitBox = new Rect();

    public CharacterSprite(Bitmap bitmap) {
        image = bitmap;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(image, x, y, null);

//        // Hitbox draw for debugging
//        Paint paint = new Paint();
//        paint.setColor(Color.argb(100, 255, 0, 0));
//        canvas.drawRect(hitBox, paint);
    }

    public void update(boolean falling) {
        // If falling
        if (falling) {
            if (yVelocity < maxSpeed) {
                yVelocity += accelerationRate;
            }

            y += yVelocity;
        }
        // If rising
        else {
            y -= jumpSpeed;
        }
    }

    public void calculateHitBox() {
        hitBox.set(x + hitBoxInwardOffset, y + hitBoxInwardOffset,
                x + GameView.characterWidth - hitBoxInwardOffset,
                y + GameView.characterHeight - hitBoxInwardOffset);
    }
}
