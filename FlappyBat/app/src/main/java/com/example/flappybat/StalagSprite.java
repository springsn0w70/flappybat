package com.example.flappybat;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class StalagSprite {
    private Bitmap image;
    private Bitmap image2;
    public int xCoord;
    public int yCoordTopStalag, yCoordBottomStalag;
    public int yOffset;
    public Rect hitBoxTopStalag = new Rect();
    public Rect hitBoxBottomStalag = new Rect();

    public StalagSprite (Bitmap bmp, Bitmap bmp2, int x, int y) {
        image = bmp;
        image2 = bmp2;
        yOffset = y;
        xCoord = x;
    }

    public void draw(Canvas canvas) {
        yCoordTopStalag = 0 - (GameView.gapHeight / 2) + yOffset;
        yCoordBottomStalag = (GameView.screenHeight / 2) + (GameView.gapHeight / 2) + yOffset;

        canvas.drawBitmap(image, xCoord, yCoordTopStalag, null);
        canvas.drawBitmap(image2, xCoord, yCoordBottomStalag, null);

//        // Hitbox draw for debugging
//        Paint paint = new Paint();
//        paint.setColor(Color.argb(100, 255, 0, 0));
//        canvas.drawRect(hitBoxTopStalag, paint);
//        canvas.drawRect(hitBoxBottomStalag, paint);
    }

    public void update() {
        xCoord -= GameView.gameVelocity;
    }

    public void calculateHitBox() {
        hitBoxTopStalag.set(xCoord, yCoordTopStalag, xCoord + GameView.stalagWidth,
                yCoordTopStalag + GameView.stalagHeight);
        hitBoxBottomStalag.set(xCoord, yCoordBottomStalag, xCoord + GameView.stalagWidth,
                yCoordBottomStalag + GameView.stalagHeight);
    }
}
