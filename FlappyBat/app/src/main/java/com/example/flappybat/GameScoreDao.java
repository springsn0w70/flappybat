package com.example.flappybat;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface GameScoreDao {
    // Sort by points descending
    @Query("SELECT * FROM gamescore ORDER BY points DESC LIMIT 15")
    List<GameScore> getAll();

    @Insert
    void insert(GameScore gameScore);

    @Delete
    void delete(GameScore gameScore);
}
