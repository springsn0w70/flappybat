package com.example.flappybat;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class PointsSprite {
    private int points;

    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.rgb(255, 255, 255));
        paint.setTextSize(60);
        canvas.drawText("Points: " + points, 100, 50, paint);
    }

    public void update(int points) {
        this.points = points;
    }
}
